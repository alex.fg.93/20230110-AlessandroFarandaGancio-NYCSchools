package com.alessandrofarandagancio.nycschools.di

import androidx.lifecycle.ViewModel
import com.alessandrofarandagancio.nycschools.network.ApiService
import com.alessandrofarandagancio.nycschools.school.viewmodel.SchoolListViewModel
import com.alessandrofarandagancio.nycschools.school.viewmodel.SchoolListViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SchoolListViewModel::class)
    abstract fun bindSchoolListViewModel(schoolListViewModel: SchoolListViewModel): ViewModel

}